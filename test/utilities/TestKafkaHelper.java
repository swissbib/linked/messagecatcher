package utilities;

import org.junit.jupiter.api.Test;
import org.swissbib.linked.kafka.helper.StatusMessageBuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class TestKafkaHelper {

    @Test
    void testStatiExchange() {

        assertEquals(StatusMessageBuilder.Status.create, SerializeKafkaHelper.convertStatiTypes(SRWAction.create));
        assertEquals(StatusMessageBuilder.Status.delete, SerializeKafkaHelper.convertStatiTypes(SRWAction.delete));
        assertEquals(StatusMessageBuilder.Status.replace, SerializeKafkaHelper.convertStatiTypes(SRWAction.replace));
        assertEquals(StatusMessageBuilder.Status.notDefined, SerializeKafkaHelper.convertStatiTypes(SRWAction.notDefined));
        assertNotEquals(StatusMessageBuilder.Status.create, SerializeKafkaHelper.convertStatiTypes(SRWAction.replace));
    }



}
