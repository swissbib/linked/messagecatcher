FROM hseeberger/scala-sbt:8u181_2.12.8_1.2.8 AS build

ADD . /
WORKDIR /
RUN sbt clean
RUN sbt stage



# Build deployable Docker image
FROM openjdk:8-jre-alpine
COPY --from=build target/universal/stage /app/

RUN apk update
RUN apk upgrade
RUN apk add bash

#see https://github.com/docker-library/openjdk/issues/289
RUN apk add --no-cache nss

VOLUME /updateDir
VOLUME /deleteDir
VOLUME /base


RUN chown -R 497:497 /app
USER 497:497
WORKDIR /app



CMD ./start.process.sh
