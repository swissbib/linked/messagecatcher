#!/usr/bin/env bash



BASE_DIR="$( cd "$(dirname "$0")" ; pwd -P )"

#TARGET_HOST=sb-dp1.swissbib.unibas.ch
TARGET_HOST=sb-uwf1.swissbib.unibas.ch
USER=swissbib
#TARGET_DIR=/swissbib_index/solrDocumentProcessing/FrequentInitialPreProcessing/catcher
TARGET_DIR=/swissbib_index/cbsCatcher

cd $BASE_DIR

echo "build latest image messagecatcher"

./buildimage.sh

echo "save latest image messagecatcher as tar file"
docker save messagecatcher --output messagecatcher.tar

echo "cp tar file to target host"
scp messagecatcher.tar $USER@$TARGET_HOST:$TARGET_DIR

echo "stop container if running on the target host"
ssh $USER@$TARGET_HOST "cd $TARGET_DIR; docker container stop messagecatcher"

echo "rm already existing image o target host"
echo "load just created image on target host"
ssh $USER@$TARGET_HOST "cd $TARGET_DIR; docker image rm messagecatcher; docker load --input messagecatcher.tar"

ssh $USER@$TARGET_HOST "cd $TARGET_DIR; rm -r conf"
#cp conf directory so we can use it via volume mapping
#scp -r conf $USER@$TARGET_HOST:$TARGET_DIR/




rm messagecatcher.tar

ssh $USER@$TARGET_HOST "rm $TARGET_DIR/messagecatcher.tar"

#docker container run -ti -p 9000:9000 messagecatcher /bin/sh

#lokal
#docker container run --network host -d -v /home/swissbib/environment/code/swissbib.repositories/messageCatcher.play:/base  messagecatcher
#docker container run  -d -v /home/swissbib/environment/code/swissbib.repositories/messageCatcher.play:/base -p 9000:9000 messagecatcher

#docker container run  -d -v /swissbib_index/cbsCatcher:/base -p 9000:9000 messagecatcher

#docker container run -ti -p 9000:9000 -v /swissbib_index/cbsCatcher:/base messagecatcher /bin/sh



