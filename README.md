# endpoint for messages sent by the CBS system (aka messageCatcher) 



The component might be used in different scenarious for further workflows after the message arrived

a) put the message into (several) Kafka clusters 

b) put the message into a file (classic worflow for swissbib)



To start the container for the Kafka workflow

```bash
docker container run  -d --rm --name messagecatcher-kafka-prod  -p 9000:9000 messagecatcher

```

Further information to start the workflow b) is going to be provided soon...

@Jonas: 

By now I'm using the buildimage.sh script for creating the docker image.

Looking into .gitlab-ci.yml should make similar things but I'm not sure - we have to see

There are several things which have to be improved 

-- use of secret key

-- code not used

-- ...

I will improve this once the component is running in production mode following the gitlab guidelines


