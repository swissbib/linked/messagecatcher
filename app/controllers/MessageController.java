package controllers;

import org.w3c.dom.Document;
import play.Environment;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utilities.*;

import javax.inject.Inject;



public class MessageController  extends Controller {

    @Inject
    Environment env;

    @BodyParser.Of(BodyParser.Xml.class)
    public Result update(Http.Request request) {

        Document dom = request.body().asXml();

        if (dom != null) {

            //get id and action type
            String id = XMLHelpers.getMessageIDFromRequest(dom);
            SRWAction action = XMLHelpers.getActionFromRequest(dom);

            //get content (bibliographic record) from soap message
            //remove all the namespace clutter and add type of the record in 001
            String record = XMLHelpers.transformRecordFromRequest(
                    XMLHelpers.getRecordFromRequest(dom)
                    );

            String normalizedIfConfigured = Helpers.normalize(record);

            String mFCompatibleRecordIfConfigured = Helpers.createCompatibleMFStructure(normalizedIfConfigured);

            if (Helpers.checkWellFormedXMLOK(mFCompatibleRecordIfConfigured)){
                //serialize record in File (if configured)
                SerializeFileHelper.serializeRecordAsFile(id,mFCompatibleRecordIfConfigured, action);

                SerializeKafkaHelper.serializeInKafka(mFCompatibleRecordIfConfigured,id,action);

                //create response
                String response = XMLHelpers.createSuccessResponse(id,
                        record
                );

                return ok(response).as("application/xml");

            } else {
                //what does CBS with such a response??
                return badRequest(XMLHelpers.createFailResponse(id,"not valid xml")).as("application/xml");
            }


        } else {
            return badRequest(XMLHelpers.createFailResponse("id not readable",
                    "not valid xml for body parser"))
                    .as("application/xml");

        }
    }
}
