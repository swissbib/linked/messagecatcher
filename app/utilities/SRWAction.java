package utilities;


/**
 * actions used in CBS messages
 * we have to analyse them because they trigger different activities and workflows
 */
public enum SRWAction {
    create("info:srw/action/1/create"),
    delete("info:srw/action/1/delete"),
    replace("info:srw/action/1/replace"),
    notDefined("not_defined");

    String action;

    SRWAction(String action) {
        this.action = action;
    }

    public String getValue() {
        return this.action;
    }

    public String getShortValue() {

        String sv;

        switch (this) {
            case create: sv = "create";
            break;
            case delete: sv = "delete";
            break;
            case replace: sv = "replace";
            break;
            default: sv = "not_defined";
        }

        return sv;
    }


    public static SRWAction fromString(String text) {
        if (text != null) {
            for (SRWAction action : SRWAction.values()) {
                if (text.equalsIgnoreCase(action.getValue())) {
                    return action;
                }
            }
        }
        return null;
    }
}
