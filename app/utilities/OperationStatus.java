package utilities;

public enum OperationStatus {
    success("success"),
    fail("fail");

    String status;

    OperationStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return this.status;
    }

    public static OperationStatus fromString(String text) {
        if (text != null) {
            for (OperationStatus status : OperationStatus.values()) {
                if (text.equalsIgnoreCase(status.getValue())) {
                    return status;
                }
            }
        }
        return null;
    }


}
