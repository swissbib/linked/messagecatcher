package utilities;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import org.swissbib.linked.kafka.KafkaWriter;
import java.io.File;
import java.util.ArrayList;
import org.swissbib.linked.kafka.helper.StatusMessageBuilder;
import static org.swissbib.linked.kafka.helper.StatusMessageBuilder.Status;



public class SerializeKafkaHelper {

    private final static Config config;
    private final static ArrayList<KafkaWriter<StatusMessageBuilder>> kwList = new ArrayList<>() ;

    static {
        config = ConfigFactory.parseFile(new File("conf/application.conf"));

        config.getObjectList("kafka.writers").forEach(configObject ->
            {
                String topic = configObject.getOrDefault("topic",
                        ConfigValueFactory.fromAnyRef("sb-all")).unwrapped().toString();
                String bootstrapservers = configObject.getOrDefault("bootstrapservers",
                        ConfigValueFactory.fromAnyRef("localhost:9092,localhost:9093,localhost:9094")).
                        unwrapped().toString();

                KafkaWriter<StatusMessageBuilder> kw = new KafkaWriter<>();
                kw.setBootstrapServers(bootstrapservers);
                kw.setKafkaTopic(topic);
                kwList.add(kw);
                });

    }

    public static void serializeInKafka(String content, String id, SRWAction status) {

        if (Boolean.parseBoolean(config.getString("kafka.kafkaSerialisation"))) {
            StatusMessageBuilder smb = new StatusMessageBuilder(content,convertStatiTypes(status));
            smb.setId(id);
            kwList.forEach(
                    writer -> writer.process(smb));
        }

    }

    static Status convertStatiTypes(SRWAction action) {
        return Status.fromString(action.getShortValue().toUpperCase());
    }


}
