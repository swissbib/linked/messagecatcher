package utilities;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.w3c.dom.Node;
import play.libs.XPath;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.regex.Pattern;

public class XMLHelpers {


    private final static Config config;
    private final static HashMap<String, String> cbsRequestNamespaces;
    private final static HashMap<String, String> cbsResponseNamespaces;
    private static Templates templateTransformer;
    private static Transformer domTransformer;
    private static String responseSuccessTemplate;
    private static String responseSuccessRecordTemplate;
    private static String responseFailTemplate;
    private static final Pattern pRecordID;
    private static final Pattern pRecord;
    private static final Pattern pTemplateFailureMessage;


    static {
        config = ConfigFactory.parseFile(new File("conf/application.conf"));
        cbsRequestNamespaces = new HashMap<>();
        cbsResponseNamespaces = new HashMap<>();
        config.getObjectList("soapnamespaces.cbsrequest").forEach(configObject ->
        {
            configObject.forEach((key,value) ->
                    cbsRequestNamespaces.put(key,value.unwrapped().toString()));
        });

        config.getObjectList("soapnamespaces.cbsresponse").forEach(configObject ->
        {
            configObject.forEach((key,value) ->
                    cbsResponseNamespaces.put(key,value.unwrapped().toString()));
        });


        try {
            StreamSource source = new StreamSource(new FileInputStream(
                    config.getString("transformRecordTemplate")
            ));
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            templateTransformer = transformerFactory.newTemplates(source);

            domTransformer = transformerFactory.newTransformer();
            //domTransformer.setOutputProperty(OutputKeys.INDENT, "yes");

        } catch (FileNotFoundException | TransformerException ex) {
            //todo: logging!
            ex.printStackTrace();

        }

        try {
            responseSuccessTemplate = new String(Files.readAllBytes(Paths.
                    get(config.getString("responseSuccessTemplate"))),
                    StandardCharsets.UTF_8);
            responseFailTemplate =  new String(Files.readAllBytes(Paths.
                    get(config.getString("responseFailureTemplate"))),
                    StandardCharsets.UTF_8);
            responseSuccessRecordTemplate = new String(Files.readAllBytes(Paths.
                    get(config.getString("responseSuccessRecordTemplate"))),
                    StandardCharsets.UTF_8);

        } catch (IOException ioException) {
            //todo: application should be stopped
            ioException.printStackTrace();
        }

        pRecordID = Pattern.compile("#RECORD_ID#",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

        pRecord =  Pattern.compile("#SINGLE_RECORD#",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

        pTemplateFailureMessage =  Pattern.compile("#FAILURE_MESSAGE#",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    }


    public static String getMessageIDFromRequest(Node dom) {
        return XPath.selectText("//mx:controlfield[@tag='001']", dom, cbsRequestNamespaces );
    }


    public static SRWAction getActionFromRequest(Node dom) {
        //todo: check correct namespace against enum!
        String actionText = XPath.selectText(config.getString("xpathAction"), dom,
                cbsRequestNamespaces );
        SRWAction action;
        try {
            action = SRWAction.fromString(actionText);
        } catch (Exception ex) {
            //todo Logging and throw customized Exception
            action = SRWAction.notDefined;
        }
        return action;


    }


    public static Node getRecordFromRequest(Node dom) {

        return XPath.selectNode(config.getString("xpathRecord"), dom, cbsRequestNamespaces );
    }


    public static String transformRecordFromRequest(Node dom) {

        String xml = getXMLStringFromNode(dom);
        StringWriter serializedRecord = new StringWriter();
        Result tempXsltResult = new StreamResult(serializedRecord);

        try {

            Source sourceWithNS = new StreamSource(new StringReader(xml));
            templateTransformer.newTransformer().transform(sourceWithNS,tempXsltResult);

        } catch (TransformerException ex) {
            //todo: logging
            ex.printStackTrace();
        }

        return serializedRecord.toString();

    }

    public static String createSuccessResponse(String id, String record) {

        String response;
        if ( !Boolean.parseBoolean(config.getString("includeRecordInResponse"))) {

            response = pRecordID.matcher(responseSuccessTemplate).replaceFirst(id);
            //response = responseSuccessTemplate.replaceAll("#RECORD_ID#",id);
        } else {
            //todo: use precompiled Patterns which is a little bit faster
            response = responseSuccessRecordTemplate.replaceAll("#RECORD_ID#",id).
                    replaceAll("#SINGLE_RECORD#",record);
        }

        return response;

    }

    public static String createFailResponse(String id, String message) {

        return responseFailTemplate.replaceAll("#RECORD_ID#",id).
                replaceAll("#FAILURE_MESSAGE#",message);
    }


    private static String getXMLStringFromNode(Node dom) {

        StringWriter writer = new StringWriter();
        try {

            DOMSource source = new DOMSource(dom);
            StreamResult result = new StreamResult(writer);
            domTransformer.transform(source, result);

        } catch (Throwable th) {
            th.printStackTrace();
        }

        return writer.toString();

    }

    /*
    just for test and as util
     */
    public static void printXML(Node dom) {
        StringWriter writer = new StringWriter();
        try {

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(dom);
            StreamResult console = new StreamResult(writer);
            transformer.transform(source, console);
            System.out.println(writer.toString());

        } catch (Throwable th) {
            th.printStackTrace();
        }

    }




}
