package utilities;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SerializeFileHelper {

    private final static Config config;
    private final static String updateDir;
    private final static String createDir;
    private final static String deleteDir;

    static {
        config = ConfigFactory.parseFile(new File("conf/application.conf"));

        //todo check for trailing slash1
        updateDir = config.getString("updateDir");
        createDir = config.getString("createDir");
        deleteDir = config.getString("deleteDir");

    }

    public static void serializeRecordAsFile(String id,
                                             String record,
                                             SRWAction action) {

        //check if file serialization is set to true
        //we check this here because config is parsed in this context
        //later we might build conditional pipes with MF
        if (Boolean.parseBoolean(config.getString("fileSerialization"))) {
            try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                    getFileForSerialization(action, id)), StandardCharsets.UTF_8))) {
                bw.write(record);
                bw.flush();
            } catch (IOException ioEx) {
                //todo loggin or another strategy
                ioEx.printStackTrace();
            }
        }

    }

    private static File getFileForSerialization (SRWAction action, String id) {

        //todo add random suffix
        String timeFilePrefix  = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());

        return new File(getOutputDir(action) + timeFilePrefix + "_REQ_" +
                id + "_" + action.name() +
                config.getString("fileSuffix"));

    }

    private static String getOutputDir (SRWAction action) {

        String outputDir = null;

        switch (action) {
            case create:
                outputDir = createDir;
                break;
            case replace:
                outputDir = updateDir;
                break;
            case delete:
                outputDir = deleteDir;
                break;
        }

        return outputDir;

    }

}
