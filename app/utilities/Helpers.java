package utilities;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.xml.sax.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.text.Normalizer;
import java.util.regex.Pattern;

public class Helpers {

    private final static Config config;

    private static final Pattern replace;
    private static final String replacement;
    private static final SAXParserFactory factory;
    //private final static SubstituteStringPattern mfConformTransportPipe;
    //private static StringWriter pipeSink;

    private static boolean checkWellFormed = false;

    static {
        config = ConfigFactory.parseFile(new File("conf/application.conf"));

        replace = Pattern.compile("<record>");
        replacement = "<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\">";

        /*
        pipeSink = new StringWriter();
        ObjectJavaIoWriter<String> ioWriter = new ObjectJavaIoWriter<>(pipeSink);
        mfConformTransportPipe = new SubstituteStringPattern();
        mfConformTransportPipe.setReplace("<record>");
        mfConformTransportPipe.setReplacement(
                "<record type=\"Bibliographic\" xmlns=\"http://www.loc.gov/MARC21/slim\">");
        mfConformTransportPipe.setReceiver(ioWriter);

         */
        checkWellFormed = config.hasPath("checkWellFormed") &&
                Boolean.parseBoolean( config.getString("checkWellFormed"));

        factory = SAXParserFactory.newInstance();
        factory.setValidating(false);
        factory.setNamespaceAware(true);


    }


    public static String normalize (String text) {

        if (Boolean.parseBoolean( config.getString("normalizeCharacters"))) {
            return Normalizer.normalize(text, Normalizer.Form.NFC);
        } else {
            return text;
        }

    }


    public static String createCompatibleMFStructure (String text) {
       //clear content in StringBuffer
        //pipeSink  = new StringWriter();
        if (Boolean.parseBoolean(config.getString("transformMFcompatible")))
            return replace.matcher(text).replaceAll(replacement);
        else
            return text;

    }

    public static boolean checkWellFormedXMLOK(String message) {

        boolean wellFormed = true;

        if (checkWellFormed) {

            try {

                SAXParser parser = factory.newSAXParser();
                XMLReader reader = parser.getXMLReader();
                //reader.setErrorHandler(new SimpleErrorHandler());
                reader.parse(new InputSource(new ByteArrayInputStream(message.getBytes())));

            } catch (ParserConfigurationException |
                    SAXException |
                    IOException ex) {
                wellFormed = false;
            }
        }

        return wellFormed;
    }

    /*
    static class SimpleErrorHandler implements ErrorHandler {
        public void warning(SAXParseException e) throws SAXException {
            System.out.println(e.getMessage());
        }

        public void error(SAXParseException e) throws SAXException {
            System.out.println(e.getMessage());
        }

        public void fatalError(SAXParseException e) throws SAXException {
            System.out.println(e.getMessage());
        }
    }
    */
}

